export const config = {
    environment: process.env.REACT_APP_NODE_ENV,
    serviceUrl: process.env.REACT_APP_SERVICE_URL || 'http://localhost:8000/',
    companyName: 'Expleo',
    secretKey:process.env.REACT_APP_SECRET_KEY,
    appBaseName:process.env.REACT_APP_BASENAME,
    appGAID:process.env.REACT_APP_GAID,
    captcha:"google",
    apiRootPath: process.env.REACT_APP_API_ROOT_PATH ||"http://localhost:8000/api",
    authenticationURL:"http://localhost:5000/authenticate",
    datauploads:"http://localhost:3322/api/",
    projects:"CCB/uploadProjects?deleteFlag=false",
    testReports:"uploadTestReports",
    components:"jira/uploadComponents",
    defects:"jira/uploadDefects/upcomingRelease",
    release:"CCB/uploadRelease?deleteFlag=false",
    requirements:"uploadRequirements/upcomingRelease",
    testresults:"uploadTests/upcomingRelease",
    userstory:"jira/uploadStories/upcomingRelease"

};
