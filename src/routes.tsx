import React from "react";
import { Redirect } from "react-router-dom";

// Layout Types
import { DefaultLayout, LoginLayout, LeftNavLayout } from "./layouts";

// Route Views
import Dashboard from "./views/dashboard/Dashboard";
import LoginApp from "./components/login/login";
import Errors from "./views/errors/Errors";
import ChangePassword from "./views/changepassword/changePassword";
import EditProfile from "./views/editprofile/editProfile";

export default [
  {
    path: "/",
    exact: true,
    layout: DefaultLayout,
    component: () => <Redirect to="/login" />
  },
  {
    path: "/login",
    layout: LoginLayout,
    component: LoginApp
  },
  {
    path: "/home",
    layout: DefaultLayout,
    component: Dashboard
  },
  {
    path: "/errors",
    layout: LeftNavLayout,
    component: Errors
  },
  {
    path: "/changepassword",
    layout: LeftNavLayout,
    component: ChangePassword
  },
  {
    path: "/editprofile",
    layout: LeftNavLayout,
    component: EditProfile
  }
];
