import React from "react";
import {
  Dropdown,
  DropdownToggle,
  DropdownMenu,
  DropdownItem,
  Collapse,
  NavItem,
  NavLink,
  Button
} from "shards-react";
import {connect} from 'react-redux';
import {logoutAction} from "../../../../redux/actions/login/loginAction";
import { LogoutOutlined } from '@ant-design/icons';

type MyProps = { logoutAction: any, loginData: {
  loginData: any,
  name: any,

}};
type MyState = { visible: boolean };
class UserActions extends React.Component<MyProps,MyState> {
  constructor(props:any) {
    super(props);

    this.state = {
      visible: false
    };

    this.toggleUserActions = this.toggleUserActions.bind(this);
  }

  toggleUserActions() {
    this.setState({
      visible: !this.state.visible
    });
  }
  handleClose = () => {
    this.props.logoutAction();
  }


  render() {
    return (
      // <NavItem tag={Dropdown} caret toggle={this.toggleUserActions}>
      //   <DropdownToggle caret tag={NavLink} className="text-nowrap px-3">
      //     <span className="d-none d-md-inline-block exp-text">Welcome {this.props.loginData.name}</span>
      //   </DropdownToggle>
      //   <Collapse tag={DropdownMenu} right small open={this.state.visible}>
      //     <DropdownItem tag='a' href="/login" className="text-danger" onclick={window.localStorage.clear()}>
      //       <i className="material-icons text-danger">&#xE879;</i> Logout
      //     </DropdownItem>
      //   </Collapse>
      // </NavItem>
      <Button type="primary"  onClick = {this.handleClose}> <div className="icon-style"><LogoutOutlined /></div></Button>
    )
  }
}
const mapStateToProps = (state:any) => ({
  loginData: state.loginData
});
export default connect(mapStateToProps,{logoutAction})(UserActions);
