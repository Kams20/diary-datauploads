import React from "react";
import {BrowserRouter,Router, Route} from "react-router-dom";
import routes from "./routes";
import withTracker from "./withTracker";
import Global from './components/layout/Global/Global';
import './App.css'
import "bootstrap/dist/css/bootstrap.min.css";
import "./assets/css/shards-dashboards.1.1.0.min.css";
import {Provider} from 'react-redux';
import store from './store';
import {config} from './config/configs'
export default() => (
    <Provider store={store}>
        <BrowserRouter basename={config.appBaseName || ""}>
          <Router history = {Global.history}>
            <div>
                {routes.map((route, index) => {
                    return (
                        <Route
                            key={index}
                            path={route.path}
                            exact={route.exact}
                            component={withTracker((props:any) => {
                            return (
                                <route.layout {...props}>
                                    <route.component {...props}/>
                                </route.layout>
                            );
                        })}/>
                    );
                })}
            </div>
            </Router>
        </BrowserRouter>
    </Provider>
);
