import {
  UPLOAD_PROJECT,
  UPLOAD_TESTRESULTS,
  UPLOAD_COMPONENTS,
  UPLOAD_DEFECTS,
  UPLOAD_RELEASE,
  UPLOAD_REQUIREMENTS,
  UPLOAD_TESTREPORTS,
  UPLOAD_USERSTORY,
} from "../types";
import fetch from "../../../utils/leoFetch";
import axios from "../../../utils/leoAxios";
import { config } from "../../../config/configs";

export const UploadProject = () => (dispatch: any, option: any) => {
  fetch(config.datauploads + config.projects, option)
    .then((res) => res.json())
    .then((projects) => {
      dispatch({
        payload: projects,
        type: UPLOAD_PROJECT,
      });
    });
};

export const UploadTestreports = () => (dispatch: any, option: any) => {
  fetch(config.datauploads + config.testReports, option)
    .then((res) => res.json())
    .then((testReports) => {
      dispatch({
        payload: testReports,
        type: UPLOAD_TESTREPORTS,
      });
    });
};

export const UploadComponents = () => (dispatch: any, option: any) => {
  fetch(config.datauploads + config.components, option)
    .then((res) => res.json())
    .then((components) => {
      dispatch({
        payload: components,
        type: UPLOAD_COMPONENTS,
      });
    });
};

export const UploadDefects = () => (dispatch: any, option: any) => {
  fetch(config.datauploads + config.defects, option)
    .then((res) => res.json())
    .then((defects) => {
      dispatch({
        payload: defects,
        type: UPLOAD_DEFECTS,
      });
    });
};

export const UploadRelease = () => (dispatch: any, option: any) => {
  fetch(config.datauploads + config.release, option)
    .then((res) => res.json())
    .then((release) => {
      dispatch({
        payload: release,
        type: UPLOAD_RELEASE,
      });
    });
};

export const Uploadrequirements = () => (dispatch: any, option: any) => {
  fetch(config.datauploads + config.requirements, option)
    .then((res) => res.json())
    .then((requirements) => {
      dispatch({
        payload: requirements,
        type: UPLOAD_REQUIREMENTS,
      });
    });
};

export const Uploadtestresults = () => (dispatch: any, option: any) => {
  fetch(config.datauploads + config.testresults, option)
    .then((res) => res.json())
    .then((testresults) => {
      dispatch({
        payload: testresults,
        type: UPLOAD_TESTRESULTS,
      });
    });
};

export const Uploaduserstory = () => (dispatch: any, option: any) => {
  fetch(config.datauploads + config.userstory, option)
    .then((res) => res.json())
    .then((userstory) => {
      dispatch({
        payload: userstory,
        type: UPLOAD_USERSTORY,
      });
    });
};
