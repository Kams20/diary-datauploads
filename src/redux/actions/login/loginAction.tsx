import {
  LOGIN,
  LOGOUT,
  CHANGE_PASSWORD,
  META_DATA,
} from "../types";
import fetch from "../../../utils/leoFetch";
import axios from "../../../utils/leoAxios";
import history from "../../../history";
// import Global from '../../../../src/components/layout/Global/Global.js';
import Global from "../../../components/layout/Global/Global";
import message from "../../../components/messages/message.json";
import { config } from "../../../config/configs";

let cryptoJs = require("crypto-js");
let jwtDecode = require("jwt-decode");

// export const logoutAction = () => (dispatch: any) => {
//   let loginData = {
//     isLoggedIn: false,
//     redirect: false,
//     token: null,
//   };
//   localStorage.clear();
//   console.log("Logout Lacal storage", localStorage);
//   dispatch({
//     payload: loginData,
//     type: LOGOUT,
//   });
// };

export const logoutAction = () => (dispatch: any) => {
  let loginData = {
    isLoggedIn: false,
    redirect: false,
    token: null
  };
  //localStorage.clear();
  dispatch({
    payload: loginData,
    type: LOGOUT
  })
  Global.history.push("/login");

}

export const loginAction = (values: any) => (dispatch: any) => {
  console.log("loginAction...........", values);

  let payload = {
    email: values.userName,
    password: values.password,
  };

  console.log("loginAction payload...........", payload);

  let loginData = {
    Name: null,
    userName: null,
    isLoggedIn: false,
    password: null, // for reference
    redirect: false,
    token: null,
  };

  axios
    .post(config.authenticationURL, payload, {
      headers: {
        "Content-Type": "application/json",
      },
    })
    .then((res) => {
      console.log(101, res.data);
        var token = res.data.token;
        if (token !== null && token !== undefined) {
          localStorage.setItem("token", token);
          localStorage.setItem("UserName", res.data.name);
          dispatch({
            payload: {
              email: res.data.email,
              name: res.data.name,
              isLoggedIn: true,
              redirect: true,
              ...res.data,
            },
            type: LOGIN,
          });
        }
      else {
        dispatch({
          payload: {
            ...loginData,
            message: message.loginErrorMessage,
          },
          type: LOGIN,
        });
      }
    });
};

export const changePassword = (values: any) => async (dispatch: any) => {
  console.log(123, values.Email);
  var options = {
    ConfirmPassword: values.ConfirmPassword,
    Email: values.Email,
    OldPassword: values.OldPassword,
  };
  console.log("To backend : ", options);
  await axios
    .put("http://localhost:8000/users/changepassword", options)
    .then((resp) => {
      if (resp.data.status === "SUCCESS") {
        Global.alertContent = {
          closable: false,
          message: resp.data.userMessage,
          status: "success",
        };
        dispatch({
          payload: {},
          type: CHANGE_PASSWORD,
        });
      } else {
        Global.alertContent = {
          closable: true,
          message: resp.data.userMessage,
          status: "error",
        };
        dispatch({
          payload: {},
          type: CHANGE_PASSWORD,
        });
      }
    });
};

export const getMetaData = (values: any) => async (dispatch: any) => {
  var options = {
    hostname: window.location.hostname,
  };
  console.log("To backend : ", options, values);
  await axios
    .post(config.apiRootPath + "/v1/getMetaData", options)
    .then((resp) => {
      var bytes = cryptoJs.AES.decrypt(resp.data["metaData"], config.secretKey);
      let data = bytes.toString(cryptoJs.enc.Utf8);
      console.log(102, data);
      let decryptedData = JSON.parse(data);
      decryptedData.style = JSON.parse(decryptedData.style);
      decryptedData.other_metadata = JSON.parse(decryptedData.other_metadata);
      dispatch({
        payload: decryptedData,
        type: META_DATA,
      });
    });
};
