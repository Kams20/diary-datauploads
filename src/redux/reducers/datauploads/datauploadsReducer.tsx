import {
  UPLOAD_PROJECT,
  UPLOAD_TESTRESULTS,
  UPLOAD_COMPONENTS,
  UPLOAD_DEFECTS,
  UPLOAD_RELEASE,
  UPLOAD_REQUIREMENTS,
  UPLOAD_TESTREPORTS,
  UPLOAD_USERSTORY,
} from "../../actions/types";

const initialState = {
  projects: {},
  testresults: {},
  components: {},
  defects: {},
  releases: {},
  requirements: {},
  testreports: {},
  userstories: {},
};

export default function (state = initialState, action: any) {
  //console.log("in reducer");
  switch (action.type) {
    case UPLOAD_PROJECT:
      return {
        ...state,
        projects: action.payload,
      };
    case UPLOAD_TESTRESULTS:
      return {
        ...state,
        testresults: action.payload,
      };
    case UPLOAD_COMPONENTS:
      return {
        ...state,
        components: action.payload,
      };
    case UPLOAD_DEFECTS:
      return {
        ...state,
        defects: action.payload,
      };
    case UPLOAD_RELEASE:
      return {
        ...state,
        releases: action.payload,
      };
    case UPLOAD_REQUIREMENTS:
      return {
        ...state,
        requirements: action.payload,
      };
    case UPLOAD_TESTREPORTS:
      return {
        ...state,
        testreports: action.payload,
      };
    case UPLOAD_USERSTORY:
      return {
        ...state,
        userstories: action.payload,
      };
    default:
      return state;
  }
}
