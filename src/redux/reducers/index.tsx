import {combineReducers} from 'redux';
import loginReducer from '../reducers/login/loginReducer';
import languageReducer from "../reducers/language/languageReducer";
import userProfileReducer from "../reducers/profileUpdateReducer/profileUpdateReducer";
import changePasswordReducer from "../reducers/changePasswordReducer/changePasswordReducer";
import metaDataReducer from "../reducers/login/metadataReducer";
import datauploadsReducer from "./datauploads/datauploadsReducer"

export default combineReducers({
    languageDatum : languageReducer,
    loginData: loginReducer,
    userProfile: userProfileReducer,
    changePassword: changePasswordReducer,
    metaData: metaDataReducer,
    datauploadsReducer: datauploadsReducer
});
