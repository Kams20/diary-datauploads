import React, { Component } from "react";
import { Button, Form, Typography } from "antd";
import { connect } from "react-redux";
import Global from "../../components/layout/Global/Global";
import {
  UploadProject,
  UploadTestreports,
  UploadComponents,
  UploadDefects,
  UploadRelease,
  Uploadrequirements,
  Uploadtestresults,
  Uploaduserstory,
} from "../../redux/actions/datauploads/datauploadsAction";

const { Text } = Typography;

type MyProps = {
  loginData: any;
  UploadProject: any;
  UploadTestreports: any;
  UploadComponents: any;
  UploadDefects: any;
  UploadRelease: any;
  Uploadrequirements: any;
  Uploadtestresults: any;
  Uploaduserstory: any
};
type MyState = {
};

class Datauploads extends Component<MyProps, MyState> {
  constructor(props: any) {
    super(props);
  }
  componentWillMount() {
    if (this.props.loginData.token === null) {
      window.localStorage.removeItem("token");
      Global.history.push("/login");
    }
  }

  dataUploads = (data: any) => {
    if(data === "projects"){
      this.props.UploadProject();
    }else if(data === "testreports"){
      this.props.UploadTestreports();
    }else if(data === "components"){
      this.props.UploadComponents();
    }else if(data === "defects"){
      this.props.UploadDefects();
    }else if(data === "release"){
      this.props.UploadRelease();
    }else if(data === "requirements"){
      this.props.Uploadrequirements();
    }else if(data === "testresults"){
      this.props.Uploadtestresults();
    }else if(data === "userstory"){
      this.props.Uploaduserstory();
    }
  }

  render() {
    return (
      <div className="container-flex page-wrapper">
        <div className="content-title">
          <Text> Welcome to Upload Data Page !!!</Text>
        </div>
        <div className="row content-align">
          <div className="col-md-3">
            <Text>Upload Projects</Text>
          </div>
          <div className="col-md-3">
            <Button type="primary" onClick = {(event:any) => this.dataUploads("projects")}>Click Here</Button>
          </div>
        </div>
        <div className="row content-align">
          <div className="col-md-3">
            <Text>Upload Test Reports</Text>
          </div>
          <div className="col-md-3">
            <Button type="primary"  onClick = {(event:any) => this.dataUploads("testreports")}>Click Here</Button>
          </div>
        </div>
        <div className="row content-align">
          <div className="col-md-3">
            <Text>Upload Components</Text>
          </div>
          <div className="col-md-3">
            <Button type="primary"  onClick = {(event:any) => this.dataUploads("components")}>Click Here</Button>
          </div>
        </div>
        <div className="row content-align">
          <div className="col-md-3">
            <Text>Upload Defects</Text>
          </div>
          <div className="col-md-3">
            <Button type="primary"  onClick = {(event:any) => this.dataUploads("defects")}>Click Here</Button>
          </div>
        </div>
        <div className="row content-align">
          <div className="col-md-3">
            <Text>Upload Release</Text>
          </div>
          <div className="col-md-3">
            <Button type="primary"  onClick = {(event:any) => this.dataUploads("release")}>Click Here</Button>
          </div>
        </div>
        <div className="row content-align">
          <div className="col-md-3">
            <Text>Upload Requirements</Text>
          </div>
          <div className="col-md-3">
            <Button type="primary"  onClick = {(event:any) => this.dataUploads("requirements")}>Click Here</Button>
          </div>
        </div>
        <div className="row content-align">
          <div className="col-md-3">
            <Text>Upload Test Results</Text>
          </div>
          <div className="col-md-3">
            <Button type="primary"  onClick = {(event:any) => this.dataUploads("testresults")}>Click Here</Button>
          </div>
        </div>
        <div className="row content-align">
          <div className="col-md-3">
            <Text>Upload User Story</Text>
          </div>
          <div className="col-md-3">
            <Button type="primary"  onClick = {(event:any) => this.dataUploads("userstory")}>Click Here</Button>
          </div>
        </div>
      </div>
    );
  }
}
const mapStateToProps = (state) => ({
  loginData: state.loginData,
});

const WrappedCreateDatauploads = Form.create({ name: "Datauploads" })(
  Datauploads
);

export default connect(mapStateToProps, {
  UploadProject,
  UploadTestreports,
  UploadComponents,
  UploadDefects,
  UploadRelease,
  Uploadrequirements,
  Uploadtestresults,
  Uploaduserstory,
})(WrappedCreateDatauploads);
