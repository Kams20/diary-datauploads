import React, { Component } from "react";
import { connect } from "react-redux";
import { Form, Select, message, Typography, Button } from "antd";
import { LogoutOutlined } from '@ant-design/icons';
import {logoutAction} from "../../redux/actions/login/loginAction";
/*Global Variable Declaration*/
const { Text } = Typography;
/*Declaring Props Variable*/
type MyProps = {logoutAction : any};

/*Declaring State Variable*/
type MyState = {};

class Header extends Component<MyProps, MyState> {
  constructor(props: any) {
    super(props);
    this.state = {};
  }

  handleClose = () => {
    this.props.logoutAction();
  }


  render() {
    return (
      <Form className="container-fluid header">
        <div className="form-group-info header-logo">
          <img
            src={require("../../assets/images/diary-logo-vector.png")}
            alt="banner"
            width="10"
            height="40"
            className="left-sec pv_logo"
          />
          <div className="header-text-div">
            <label className="right-sec header-txt">
              <b>
                <Text className="exp-text">® Expleo </Text>Dynamic Integrated
                Automated Reporting for You
              </b>
            </label>
          </div>
        </div>
        <div style={{ float: "right", marginLeft: "auto" }}>
          <Button
            className="header-btn header-btn-cancel"
            onClick={this.handleClose}
          >
            Logout
            {/* <LogoutOutlined /> */}
          </Button>
        </div>
      </Form>
    );
  }
}

const mapStateToProps = (state) => ({});

const WrappedCreateHeader = Form.create({ name: "header" })(Header);

export default connect(mapStateToProps, {logoutAction})(WrappedCreateHeader);
